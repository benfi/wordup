<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy, używanej lokalizacji WordPressa
 * i ABSPATH. Więćej informacji znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
define('DB_NAME', 'wordup');

/** Nazwa użytkownika bazy danych MySQL */
define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
define('DB_PASSWORD', 'benfi');

/** Nazwa hosta serwera MySQL */
define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+sb+FHX1%If= |^MH0Hy,1H;Ti~2!gR@8DavF.:_X^+eytO$UVB>N0nsCR:PZ5mA');
define('SECURE_AUTH_KEY',  '|1Twi.q{bQZapPI|z>a}Eoe-b: iY#LUzWr_{2Y{E=1}^w4b]+__#4/1io%}y}s%');
define('LOGGED_IN_KEY',    'Hlz/s%=Pp`o^Z=I[7o#/,[?6eFm(OkN[`yUr[+!]%~HNE!DQ0I4itdY2u!|9f?CX');
define('NONCE_KEY',        '[-?:p]t!Tx|-GN!)Ezf?C)$/q#(! ]I:A~0fK?F^$ayKIiFHiEn$}>k8j!a]6,iV');
define('AUTH_SALT',        'fvL&+NE0~kxB F8(y^zblyX8l*JnC#-7P0!s+1J7yQm0o#}k+WGcI_Z)2Nf9GGr~');
define('SECURE_AUTH_SALT', 'o5s,rk#@!s_otD8_BZ~ umk$xmJ|.`m[iA}8d_f#ml^=Fsox%{P]-CUk!%@^16B@');
define('LOGGED_IN_SALT',   'Vdx7]B2E&vo^C@?!x.yp?fn!ypB%r~SHDlDjuW/_ao(M*|Sna2Gk$y($W8d8Xrq%');
define('NONCE_SALT',       '!&/=;Xg[dppVd`rnx ay;MD]x{b@R-bCx!Bk`(X$Yqs95( .nlBEwI<^5CFFKvyu');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'wp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');
